# Package

version       = "0.1.0"
author        = "RoFlush"
description   = "Charter Lang"
license       = "GPL-3.0-only"
srcDir        = "src"
bin           = @["charterlang"]


# Dependencies

requires "nim >= 1.0.0"
requires "docopt >= 0.6.7"